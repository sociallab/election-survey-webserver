using System.Web.Mvc;
using Microsoft.Practices.Unity;
using ElectionWebServerBot.Models.Repositories;
using ElectionWebServerBot.Models.DomainModel;
using Microsoft.Practices.ServiceLocation;
using Unity.Mvc4;

namespace ElectionWebServerBot
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IUnitOfWork, ElectionBotEntities1>();
           // container.RegisterType<IUnitOfWork>().Use(() => new ElectionBotEntities1());
            container.RegisterType<IRoleRepository, RoleRepository>();
            container.RegisterType<ISatrapInRoleRepository, SatrapInRoleRepository>();
            container.RegisterType<ICandidateRepository, CandidateRepository>();
            container.RegisterType<ISatrapRepository, SatrapRepository>();
            container.RegisterType<IStateRepository, StateRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IBotStateRepository, BotStateRepository>();
            container.RegisterType<ICityRepository, CityRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(container));
        }
    }
}