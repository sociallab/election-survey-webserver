﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class BotStatesRepository : IDisposable
    {
           private ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public BotStatesRepository()
        {
            db = new DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.BotState> entity, bool autoSave = true)
        {
            try
            {

                db.BotStates.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.BotState> entity, bool autoSave = true)
        {
            try
            {
                db.BotStates.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public int Add(ElectionWebServerBot.Models.DomainModel.BotState entity, bool autoSave = true)
        {
            try
            {
                db.BotStates.Add(entity);
                if (autoSave)
                  if(Convert.ToBoolean( db.SaveChanges()))
                    return entity.Id;
                else
                    return -1;
                else  return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(ElectionWebServerBot.Models.DomainModel.BotState entity, bool autoSave = true)
        {
            try
            {
                db.BotStates.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.BotState entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.BotStates.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.BotState Find(int id)
        {
            try
            {
                return db.BotStates.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.BotState> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.BotState, bool>> predicate)
        {
            try
            {
                return db.BotStates.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.BotState> Select()
        {
            try
            {
                return db.BotStates.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.BotState, TResult>> selector)
        {
            try
            {
                return db.BotStates.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public int GetLastIdentity()
        {
            try
            {
                if (db.BotStates.Any())
                    return db.BotStates.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }

        ~BotStatesRepository()
        {
            Dispose(false);
        }
    }
}