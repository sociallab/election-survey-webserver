﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class UsersRepository : IDisposable
    {
           public ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public UsersRepository()
        {
            db = new DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.User> entity, bool autoSave = true)
        {
            try
            {

                db.Users.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.User> entity, bool autoSave = true)
        {
            try
            {
                db.Users.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public long Add(ElectionWebServerBot.Models.DomainModel.User entity, bool autoSave = true)
        {
            try
            {
                db.Users.Add(entity);
                if (autoSave)
                  if(Convert.ToBoolean( db.SaveChanges()))
                    return entity.Id;
                else
                    return -1;
                else  return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(ElectionWebServerBot.Models.DomainModel.User entity, bool autoSave = true)
        {
            try
            {
                db.Users.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.User entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.Users.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.User Find(int id)
        {
            try
            {
                return db.Users.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.User> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.User, bool>> predicate)
        {
            try
            {
                return db.Users.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.User> Select()
        {
            try
            {
                return db.Users.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.User, TResult>> selector)
        {
            try
            {
                return db.Users.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (db.Users.Any())
                    return db.Users.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }

        ~UsersRepository()
        {
            Dispose(false);
        }
    }
}