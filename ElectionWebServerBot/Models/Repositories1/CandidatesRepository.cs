﻿using ElectionWebServerBot.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class CandidatesRepository : IDisposable
    {
        public ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public CandidatesRepository()
        {
            db = new DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.Candidate> entity, bool autoSave = true)
        {
            try
            {

                db.Candidates.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.Candidate> entity, bool autoSave = true)
        {
            try
            {
                db.Candidates.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public int Add(ElectionWebServerBot.Models.DomainModel.Candidate entity, bool autoSave = true)
        {
            try
            {
                db.Candidates.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(db.SaveChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }
        public bool addCandidate(Candidate candidate, HttpPostedFileBase UploadImage, string UserName, string Password, string path)
        {
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    string fileName = "CandidatePhoto" + Guid.NewGuid() + Path.GetExtension(UploadImage.FileName);
                    UploadImage.InputStream.ResizeImageByWidth(500, path + fileName, Utilty.ImageComperssion.Normal);
                    candidate.Pic = fileName;
                    var cid = Add(candidate);
                    if (cid != -1)
                    {
                        var satrap = new Satrap()
                        {
                            Email = candidate.Email,
                            StrapName = UserName,
                            SatrapType = 1,
                            CandidateID = cid,
                            Enable = true
                        };
                        var salt = Helpers.Security.SecurityMethodes.GenerateSalt();

                        var hashedPassword = Helpers.Security.SecurityMethodes.HashPassword(
                                              Encoding.UTF8.GetBytes(Password),
                                              salt,
                                              50001);
                        satrap.SatrapObor = Convert.ToBase64String(hashedPassword);
                        satrap.SatrapRndCode = Convert.ToBase64String(salt);
                        db.Satraps.Add(satrap);
                        if (Convert.ToBoolean(db.SaveChanges()))
                        {
                            db.SatrapInRoles.Add(new SatrapInRole() { RoleId = 2, SatrapId = satrap.Id });
                            if (Convert.ToBoolean(db.SaveChanges()))
                            {
                                dbContextTransaction.Commit();
                                return true;
                            }
                            else
                            {
                                try
                                {
                                    System.IO.File.Delete(path);
                                }
                                catch (Exception)
                                {


                                }
                                dbContextTransaction.Rollback();
                                return false;
                            }

                        }
                        else
                        {
                            try
                            {
                                System.IO.File.Delete(path);
                            }
                            catch (Exception)
                            {


                            }
                            dbContextTransaction.Rollback();
                            return false;
                        }
                    }
                    else
                    {
                        try
                        {
                            System.IO.File.Delete(path);
                        }
                        catch (Exception)
                        {


                        }
                        dbContextTransaction.Rollback();
                        return false;
                    }
                }
                catch (Exception)
                {
                    try
                    {
                        System.IO.File.Delete(path);
                    }
                    catch (Exception)
                    {


                    }

                    dbContextTransaction.Rollback();
                    return false;
                }
            }
        }
        public bool Update(ElectionWebServerBot.Models.DomainModel.Candidate entity, bool autoSave = true)
        {
            try
            {
                db.Candidates.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.Candidate entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.Candidates.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.Candidate Find(int id)
        {
            try
            {
                return db.Candidates.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.Candidate> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Candidate, bool>> predicate)
        {
            try
            {
                return db.Candidates.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.Candidate> Select()
        {
            try
            {
                return db.Candidates.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Candidate, TResult>> selector)
        {
            try
            {
                return db.Candidates.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public int GetLastIdentity()
        {
            try
            {
                if (db.Candidates.Any())
                    return db.Candidates.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }

        ~CandidatesRepository()
        {
            Dispose(false);
        }
    }
}