﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class SatrapInRolesRepository : IDisposable
    {
        private ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public SatrapInRolesRepository()
        {
            db = new ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.SatrapInRole> entity, bool autoSave = true)
        {
            try
            {

                db.SatrapInRoles.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.SatrapInRole> entity, bool autoSave = true)
        {
            try
            {
                db.SatrapInRoles.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public int Add(ElectionWebServerBot.Models.DomainModel.SatrapInRole entity, bool autoSave = true)
        {
            try
            {
                db.SatrapInRoles.Add(entity);
                if (autoSave)
                  if(Convert.ToBoolean( db.SaveChanges()))
                    return entity.id;
                else
                    return -1;
                else  return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(ElectionWebServerBot.Models.DomainModel.SatrapInRole entity, bool autoSave = true)
        {
            try
            {
                db.SatrapInRoles.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.SatrapInRole entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.SatrapInRoles.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.SatrapInRole Find(int id)
        {
            try
            {
                return db.SatrapInRoles.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.SatrapInRole> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.SatrapInRole, bool>> predicate)
        {
            try
            {
                return db.SatrapInRoles.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.SatrapInRole> Select()
        {
            try
            {
                return db.SatrapInRoles.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.SatrapInRole, TResult>> selector)
        {
            try
            {
                return db.SatrapInRoles.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public int GetLastIdentity()
        {
            try
            {
                if (db.SatrapInRoles.Any())
                    return db.SatrapInRoles.OrderByDescending(p => p.id).First().id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public bool AddSatrapInRoll(ElectionWebServerBot.Models.DomainModel.Satrap satrap,int roleid)
        {
            using (var dbcontexttransaction=db.Database.BeginTransaction())
            {
                try
                {
                    db.Satraps.Add(satrap);
                    if ( Convert.ToBoolean(db.SaveChanges()))
                    {
                        db.SatrapInRoles.Add(new ElectionWebServerBot.Models.DomainModel.SatrapInRole() { SatrapId = satrap.Id, RoleId = roleid });
                        db.SaveChanges();
                    }
                    else
                    {
                        dbcontexttransaction.Rollback();
                        return false;
                    }
                    dbcontexttransaction.Commit();
                    return true;
                }
                catch (Exception)
                {

                    dbcontexttransaction.Rollback();
                    return false;
                }
                
            }
        }
        public bool DeleteSatrapInRollAndSatrap(int id)
        {
            using (var dbcontexttransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.SatrapInRoles.RemoveRange(db.SatrapInRoles.Where(x=>x.SatrapId==id));
                    if (Convert.ToBoolean(db.SaveChanges()))
                    {
                        db.Satraps.Remove(db.Satraps.Find(id));
                        db.SaveChanges();
                    }
                    else
                    {
                        dbcontexttransaction.Rollback();
                        return false;
                    }
                    dbcontexttransaction.Commit();
                    return true;
                }
                catch (Exception)
                {

                    dbcontexttransaction.Rollback();
                    return false;
                }

            }
        }
        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }

        ~SatrapInRolesRepository()
        {
            Dispose(false);
        }
    }

}