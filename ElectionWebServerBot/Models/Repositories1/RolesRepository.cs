﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class RolesRepository : IDisposable
    {
           private ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public RolesRepository()
        {
            db = new DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.Role> entity, bool autoSave = true)
        {
            try
            {

                db.Roles.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.Role> entity, bool autoSave = true)
        {
            try
            {
                db.Roles.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public int Add(ElectionWebServerBot.Models.DomainModel.Role entity, bool autoSave = true)
        {
            try
            {
                db.Roles.Add(entity);
                if (autoSave)
                  if(Convert.ToBoolean( db.SaveChanges()))
                    return entity.Id;
                else
                    return -1;
                else  return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(ElectionWebServerBot.Models.DomainModel.Role entity, bool autoSave = true)
        {
            try
            {
                db.Roles.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.Role entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.Roles.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.Role Find(int id)
        {
            try
            {
                return db.Roles.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.Role> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Role, bool>> predicate)
        {
            try
            {
                return db.Roles.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.Role> Select()
        {
            try
            {
                return db.Roles.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Role, TResult>> selector)
        {
            try
            {
                return db.Roles.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public int GetLastIdentity()
        {
            try
            {
                if (db.Roles.Any())
                    return db.Roles.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }

        ~RolesRepository()
        {
            Dispose(false);
        }
    }
}