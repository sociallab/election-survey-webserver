﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class CityRepository : IDisposable
    {
        private ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public CityRepository()
        {
            db = new ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.City> entity, bool autoSave = true)
        {
            try
            {

                db.Cities.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.City> entity, bool autoSave = true)
        {
            try
            {
                db.Cities.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public long Add(ElectionWebServerBot.Models.DomainModel.City entity, bool autoSave = true)
        {
            try
            {
                db.Cities.Add(entity);
                if (autoSave)
                  if(Convert.ToBoolean( db.SaveChanges()))
                    return entity.Id;
                else
                    return -1;
                else  return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(ElectionWebServerBot.Models.DomainModel.City entity, bool autoSave = true)
        {
            try
            {
                db.Cities.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.City entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.Cities.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.City Find(long id)
        {
            try
            {
                return db.Cities.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.City> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.City, bool>> predicate)
        {
            try
            {
                return db.Cities.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.City> Select()
        {
            try
            {
                return db.Cities.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.City, TResult>> selector)
        {
            try
            {
                return db.Cities.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (db.Cities.Any())
                    return db.Cities.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }
        
        ~CityRepository()
        {
            Dispose(false);
        }

    }
}