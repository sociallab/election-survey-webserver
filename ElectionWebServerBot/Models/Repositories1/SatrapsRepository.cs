﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ElectionWebServerBot.Models.Repositories
{
    public class SatrapsRepository : IDisposable
    {
        private ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = null;

        public SatrapsRepository()
        {
            db = new DomainModel.ElectionBotEntities1();
        }
        public bool DeleteRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.Satrap> entity, bool autoSave = true)
        {
            try
            {

                db.Satraps.RemoveRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public bool AddRange(IEnumerable<ElectionWebServerBot.Models.DomainModel.Satrap> entity, bool autoSave = true)
        {
            try
            {
                db.Satraps.AddRange(entity);
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public int Add(ElectionWebServerBot.Models.DomainModel.Satrap entity, bool autoSave = true)
        {
            try
            {
                db.Satraps.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(db.SaveChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(ElectionWebServerBot.Models.DomainModel.Satrap entity, bool autoSave = true)
        {
            try
            {
                db.Satraps.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(ElectionWebServerBot.Models.DomainModel.Satrap entity, bool autoSave = true)
        {
            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = db.Satraps.Find(id);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                if (autoSave)
                    return Convert.ToBoolean(db.SaveChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public ElectionWebServerBot.Models.DomainModel.Satrap Find(int id)
        {
            try
            {
                return db.Satraps.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.Satrap> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Satrap, bool>> predicate)
        {
            try
            {
                return db.Satraps.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<ElectionWebServerBot.Models.DomainModel.Satrap> Select()
        {
            try
            {
                return db.Satraps.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Satrap, TResult>> selector)
        {
            try
            {
                return db.Satraps.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public int GetLastIdentity()
        {
            try
            {
                if (db.Satraps.Any())
                    return db.Satraps.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch
            {
                return -1;
            }
        }
        public bool Exist(string username, string password)
        {
            try
            {
                var satrap = db.Satraps.Where(x => x.StrapName == username).SingleOrDefault();

                if (satrap == null)
                {
                    //pid = null;
                    return false;
                }
                else
                {
                    //pid = (satrap.SatrapType == (int)SatrapType.PishKhan && satrap.Pishkhan_Id.HasValue) ? satrap.Pishkhan_Id : null;
                    var salt = satrap.SatrapRndCode;
                    var hashedPassword = Helpers.Security.SecurityMethodes.HashPassword(
                      Encoding.UTF8.GetBytes(password),
                      Convert.FromBase64String(salt),
                      50001);
                    if (Convert.ToBase64String(hashedPassword) == satrap.SatrapObor)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                //pid = null;
                return false;
            }
        }
        public ElectionWebServerBot.Models.DomainModel.Satrap GetClientWithToken(string theBytes, string token)
        {
            var Client = db.Satraps.Where(x => x.TheBytes == theBytes && x.Token == token).SingleOrDefault();
            if (Client != null)
            {
                return Client;
            }
            else
            {
                return null;
            }
        }
        public ElectionWebServerBot.Models.DomainModel.Satrap GetClientWithPassword(string Username, string password)
        {
            try
            {
                var Client = db.Satraps.Where(x => x.StrapName == Username).SingleOrDefault();

                if (Client == null)
                {
                    //pid = null;
                    return null;
                }
                else
                {
                    //pid = (satrap.SatrapType == (int)SatrapType.PishKhan && satrap.Pishkhan_Id.HasValue) ? satrap.Pishkhan_Id : null;
                    var salt = Client.SatrapRndCode;
                    var hashedPassword = Helpers.Security.SecurityMethodes.HashPassword(
                      Encoding.UTF8.GetBytes(password),
                      Convert.FromBase64String(salt),
                      50001);
                    if (Convert.ToBase64String(hashedPassword) == Client.SatrapObor)
                    {
                        return Client;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch
            {
                //pid = null;
                return null;
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    this.db.Dispose();
                    this.db = null;
                }
            }
        }

        ~SatrapsRepository()
        {
            Dispose(false);
        }
    }
}