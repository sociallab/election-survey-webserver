﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.EntityModels
{
    public class CandidateMetaData
    {
        [Required(ErrorMessage = "نام و نام خانوادگی کاربری را وارد کنید")]
        [Display(Name = "نام و نام خانوادگی")]
        [DisplayName("نام و نام خانوادگی")]
        public string FullName { get; set; }
        [ScaffoldColumn(false)]
        [Bindable(false)]
        public byte StateID { get; set; }
        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int CityId { get; set; }
        [Display(Name = "درباره")]
        [DisplayName("درباره")]
        public string Description { get; set; }
        [Display(Name = "تحصیلات")]
        [DisplayName("تحصیلات")]
        public string Education { get; set; }
        [Display(Name = "پیام")]
        [DisplayName("پیام")]
        public string Message { get; set; }
        [Display(Name = "تاریخ")]
        [DisplayName("تاریخ")]
        public string History { get; set; }
        [Display(Name = "عکس")]
        [DisplayName("عکس")]
        public string Pic { get; set; }
        [Display(Name = "حزب سیاسی")]
        [DisplayName("حزب سیاسی")]
        public string Party { get; set; }
        [Display(Name = "اطلاعات تماس")]
        [DisplayName("اطلاعات تماس")]
        public string ContactInfos { get; set; }
        //[Required(ErrorMessage = "ایمیل خود را وارد کنید")]
        [Display(Name = "ایمیل")]
        [DisplayName("ایمیل")]
        [RegularExpression(@"^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$", ErrorMessage = "ایمیل را بدرستی وارد کنید")]
        [StringLength(50, ErrorMessage = "این فیلد باید حداکثر 50 کاراکتر باشد")]
        public string Email { get; set; }
        [Required(ErrorMessage = "وضعیت کاندید را مشخص کنید")]
        [Display(Name = "وضعیت")]
        [DisplayName("وضعیت")]
        public bool Enable { get; set; }


    }
}
namespace ElectionWebServerBot.Models.DomainModel
{
    [MetadataType(typeof(ElectionWebServerBot.Models.EntityModels.CandidateMetaData))]
    public partial class Candidate
    {

    }
}