﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Models.EntityModels
{
    public class SatrapMetaData
    {
        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "نام کاربری را وارد کنید")]
        [Display(Name = "نام کاربری")]
        [DisplayName("نام کاربری")]
        [StringLength(50, ErrorMessage = "این فیلد باید حداکثر 50 کاراکتر باشد")]
        public string StrapName { get; set; }
        //[Required(ErrorMessage = "ایمیل خود را وارد کنید")]
        [Display(Name = "ایمیل")]
        [DisplayName("ایمیل")]
        [RegularExpression(@"^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$", ErrorMessage = "ایمیل را بدرستی وارد کنید")]
        [StringLength(50, ErrorMessage = "این فیلد باید حداکثر 50 کاراکتر باشد")]
        public string Email { get; set; }
        [DisplayName("نام کاربر")]
        [Display(Name = "نام کاربر")]
        [StringLength(50, ErrorMessage = "این فیلد باید حداکثر 50 کاراکتر باشد")]
        public string KarbarName { get; set; }
       
        [Required(AllowEmptyStrings = false, ErrorMessage = "رمز عبور خود را وارد کنید")]
        [DisplayName("رمز عبور")]
        [Display(Name = "رمز عبور")]
        [DataType(DataType.Password)]
        public string SatrapObor { get; set; }
        [DisplayName("شماره تلفن")]
        [Display(Name = "شماره تلفن")]
        public string Phone { get; set; }
        [DisplayName("موبایل")]
        [Display(Name = "موبایل")]
        public string Mobile { get; set; }
        [DisplayName("نام شرکت")]
        [Display(Name = "نام شرکت")]
        public string CompanyName { get; set; }
        [DisplayName("آدرس")]
        [Display(Name = "آدرس")]
        public string Address { get; set; }


        [ScaffoldColumn(false)]
        public byte SatrapType { get; set; }

        [ScaffoldColumn(false)]
        public string SatrapRndCode { get; set; }


        [Required(ErrorMessage = "فعال یا غیره فعال بودن کاربر مشخص کنید")]
        [DisplayName("وضعیت فعال بودن یا نبودن")]
        [Display(Name = "وضعیت فعال بودن یا نبودن")]
        public bool Enable { get; set; }


    }
}
namespace ElectionWebServerBot.Models.DomainModel
{
    [MetadataType(typeof(ElectionWebServerBot.Models.EntityModels.SatrapMetaData))]
    public partial class Satrap
    {

    }
}