//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ElectionWebServerBot.Models.DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Satrap
    {
        public Satrap()
        {
            this.SatrapInRoles = new HashSet<SatrapInRole>();
        }
    
        public int Id { get; set; }
        public string StrapName { get; set; }
        public string KarbarName { get; set; }
        public string KarbarFamily { get; set; }
        public string Email { get; set; }
        public string SatrapObor { get; set; }
        public byte SatrapType { get; set; }
        public string SatrapRndCode { get; set; }
        public bool Enable { get; set; }
        public string Token { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string CompanyName { get; set; }
        public string TheBytes { get; set; }
        public Nullable<bool> IsOnline { get; set; }
        public string Address { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> CandidateID { get; set; }
    
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<SatrapInRole> SatrapInRoles { get; set; }
    }
}
