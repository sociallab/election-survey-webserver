﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class StateRepository : ElectionWebServerBot.Models.Repositories.IStateRepository
    {
        IUnitOfWork _uow;
        readonly IDbSet<State> _State;

        public StateRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _State = uow.Set<ElectionWebServerBot.Models.DomainModel.State>();
        }
        public long Add(State entity, bool autoSave = true)
        {
            try
            {
                _State.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(State entity, bool autoSave = true)
        {
            try
            {
                _State.Attach(entity);
                _uow.MarkAsChanged<State>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(State entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<State>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _State.Find(id);
                _uow.MarkAsDeleted<State>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public State Find(long id)
        {
            try
            {
                return _State.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<State> Where(System.Linq.Expressions.Expression<Func<State, bool>> predicate)
        {
            try
            {
                return _State.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<State> Select()
        {
            try
            {
                return _State.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<State, TResult>> selector)
        {
            try
            {
                return _State.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_State.Any())
                    return _State.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}