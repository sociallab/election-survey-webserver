﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface ISatrapInRoleRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.SatrapInRole entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.SatrapInRole entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.SatrapInRole Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.SatrapInRole> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.SatrapInRole, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.SatrapInRole entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.SatrapInRole> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.SatrapInRole, bool>> predicate);
    }
}
