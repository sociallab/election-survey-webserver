﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface IStateRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.State entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.State entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.State Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.State> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.State, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.State entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.State> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.State, bool>> predicate);
    }
}
