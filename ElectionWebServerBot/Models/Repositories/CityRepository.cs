﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class CityRepository : ElectionWebServerBot.Models.Repositories.ICityRepository 
    {
        IUnitOfWork _uow;
        readonly IDbSet<City> _City;

        public CityRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _City = uow.Set<ElectionWebServerBot.Models.DomainModel.City>();
        }
        public long Add(City entity, bool autoSave = true)
        {
            try
            {
                _City.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(City entity, bool autoSave = true)
        {
            try
            {
                _City.Attach(entity);
                _uow.MarkAsChanged<City>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(City entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<City>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _City.Find(id);
                _uow.MarkAsDeleted<City>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public City Find(long id)
        {
            try
            {
                return _City.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<City> Where(System.Linq.Expressions.Expression<Func<City, bool>> predicate)
        {
            try
            {
                return _City.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<City> Select()
        {
            try
            {
                return _City.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<City, TResult>> selector)
        {
            try
            {
                return _City.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_City.Any())
                    return _City.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}