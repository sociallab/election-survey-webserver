﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface IRoleRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.Role entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.Role entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.Role Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.Role> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Role, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.Role entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.Role> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Role, bool>> predicate);
    }
}
