﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface IUserRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.User entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.User entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.User Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.User> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.User, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.User entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.User> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.User, bool>> predicate);
    }
}
