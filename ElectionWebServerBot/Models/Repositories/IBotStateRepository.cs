﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface IBotStateRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.BotState entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.BotState entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.BotState Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.BotState> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.BotState, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.BotState entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.BotState> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.BotState, bool>> predicate);
    }
}
