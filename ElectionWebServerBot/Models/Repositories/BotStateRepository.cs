﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class BotStateRepository : IBotStateRepository
    {
        IUnitOfWork _uow;
        readonly IDbSet<BotState> _BotState;

        public BotStateRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _BotState = uow.Set<ElectionWebServerBot.Models.DomainModel.BotState>();
        }
        public long Add(BotState entity, bool autoSave = true)
        {
            try
            {
                _BotState.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(BotState entity, bool autoSave = true)
        {
            try
            {
                _BotState.Attach(entity);
                _uow.MarkAsChanged<BotState>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(BotState entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<BotState>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _BotState.Find(id);
                _uow.MarkAsDeleted<BotState>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public BotState Find(long id)
        {
            try
            {
                return _BotState.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<BotState> Where(System.Linq.Expressions.Expression<Func<BotState, bool>> predicate)
        {
            try
            {
                return _BotState.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<BotState> Select()
        {
            try
            {
                return _BotState.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<BotState, TResult>> selector)
        {
            try
            {
                return _BotState.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_BotState.Any())
                    return _BotState.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}