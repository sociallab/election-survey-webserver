﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class UserRepository : ElectionWebServerBot.Models.Repositories.IUserRepository
    {
        IUnitOfWork _uow;
        readonly IDbSet<User> _User;

        public UserRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _User = uow.Set<ElectionWebServerBot.Models.DomainModel.User>();
        }
        public long Add(User entity, bool autoSave = true)
        {
            try
            {
                _User.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(User entity, bool autoSave = true)
        {
            try
            {
                _User.Attach(entity);
                _uow.MarkAsChanged<User>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(User entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<User>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _User.Find(id);
                _uow.MarkAsDeleted<User>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public User Find(long id)
        {
            try
            {
                return _User.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<User> Where(System.Linq.Expressions.Expression<Func<User, bool>> predicate)
        {
            try
            {
                return _User.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<User> Select()
        {
            try
            {
                return _User.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<User, TResult>> selector)
        {
            try
            {
                return _User.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_User.Any())
                    return _User.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}