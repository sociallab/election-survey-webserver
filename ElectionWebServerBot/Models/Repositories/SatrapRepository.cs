﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;
using System.Text;

namespace ElectionWebServerBot.Models.Repositories
{
    public class SatrapRepository : ElectionWebServerBot.Models.Repositories.ISatrapRepository 
    {
        IUnitOfWork _uow;
        readonly IDbSet<Satrap> _Satrap;

        public SatrapRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _Satrap = uow.Set<ElectionWebServerBot.Models.DomainModel.Satrap>();
        }
        public bool Exist(string username, string password)
        {
            try
            {
                var satrap = _Satrap.Where(x => x.StrapName == username).SingleOrDefault();

                if (satrap == null)
                {
                    //pid = null;
                    return false;
                }
                else
                {
                    //pid = (satrap.SatrapType == (int)SatrapType.PishKhan && satrap.Pishkhan_Id.HasValue) ? satrap.Pishkhan_Id : null;
                    var salt = satrap.SatrapRndCode;
                    var hashedPassword = Helpers.Security.SecurityMethodes.HashPassword(
                      Encoding.UTF8.GetBytes(password),
                      Convert.FromBase64String(salt),
                      50001);
                    if (Convert.ToBase64String(hashedPassword) == satrap.SatrapObor)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                //pid = null;
                return false;
            }
        }
        public long Add(Satrap entity, bool autoSave = true)
        {
            try
            {
                _Satrap.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(Satrap entity, bool autoSave = true)
        {
            try
            {
                _Satrap.Attach(entity);
                _uow.MarkAsChanged<Satrap>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Satrap entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<Satrap>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _Satrap.Find(id);
                _uow.MarkAsDeleted<Satrap>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public Satrap Find(long id)
        {
            try
            {
                return _Satrap.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<Satrap> Where(System.Linq.Expressions.Expression<Func<Satrap, bool>> predicate)
        {
            try
            {
                return _Satrap.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<Satrap> Select()
        {
            try
            {
                return _Satrap.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<Satrap, TResult>> selector)
        {
            try
            {
                return _Satrap.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_Satrap.Any())
                    return _Satrap.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}