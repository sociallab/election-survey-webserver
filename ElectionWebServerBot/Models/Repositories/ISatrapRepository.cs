﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface ISatrapRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.Satrap entity, bool autoSave = true);
        bool Exist(string username, string password);
        bool Delete(ElectionWebServerBot.Models.DomainModel.Satrap entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.Satrap Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.Satrap> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Satrap, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.Satrap entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.Satrap> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Satrap, bool>> predicate);
    }
}
