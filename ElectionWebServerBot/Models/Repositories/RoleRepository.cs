﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class RoleRepository : ElectionWebServerBot.Models.Repositories.IRoleRepository 
    {
        IUnitOfWork _uow;
        readonly IDbSet<Role> _Role;

        public RoleRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _Role = uow.Set<ElectionWebServerBot.Models.DomainModel.Role>();
        }
        public long Add(Role entity, bool autoSave = true)
        {
            try
            {
                _Role.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(Role entity, bool autoSave = true)
        {
            try
            {
                _Role.Attach(entity);
                _uow.MarkAsChanged<Role>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Role entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<Role>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _Role.Find(id);
                _uow.MarkAsDeleted<Role>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public Role Find(long id)
        {
            try
            {
                return _Role.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<Role> Where(System.Linq.Expressions.Expression<Func<Role, bool>> predicate)
        {
            try
            {
                return _Role.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<Role> Select()
        {
            try
            {
                return _Role.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<Role, TResult>> selector)
        {
            try
            {
                return _Role.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_Role.Any())
                    return _Role.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}