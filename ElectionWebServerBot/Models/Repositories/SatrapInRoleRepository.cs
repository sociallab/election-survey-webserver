﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class SatrapInRoleRepository : ElectionWebServerBot.Models.Repositories.ISatrapInRoleRepository
    {
        IUnitOfWork _uow;
        readonly IDbSet<SatrapInRole> _SatrapInRole;

        public SatrapInRoleRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _SatrapInRole = uow.Set<ElectionWebServerBot.Models.DomainModel.SatrapInRole>();
        }
        public long Add(SatrapInRole entity, bool autoSave = true)
        {
            try
            {
                _SatrapInRole.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(SatrapInRole entity, bool autoSave = true)
        {
            try
            {
                _SatrapInRole.Attach(entity);
                _uow.MarkAsChanged<SatrapInRole>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(SatrapInRole entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<SatrapInRole>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _SatrapInRole.Find(id);
                _uow.MarkAsDeleted<SatrapInRole>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public SatrapInRole Find(long id)
        {
            try
            {
                return _SatrapInRole.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<SatrapInRole> Where(System.Linq.Expressions.Expression<Func<SatrapInRole, bool>> predicate)
        {
            try
            {
                return _SatrapInRole.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<SatrapInRole> Select()
        {
            try
            {
                return _SatrapInRole.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<SatrapInRole, TResult>> selector)
        {
            try
            {
                return _SatrapInRole.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_SatrapInRole.Any())
                    return _SatrapInRole.OrderByDescending(p => p.id).First().id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}