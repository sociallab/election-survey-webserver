﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface ICityRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.City entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.City entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.City Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.City> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.City, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.City entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.City> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.City, bool>> predicate);
    }
}
