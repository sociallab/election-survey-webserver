﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ElectionWebServerBot.Models.DomainModel;

namespace ElectionWebServerBot.Models.Repositories
{
    public class CandidateRepository : ElectionWebServerBot.Models.Repositories.ICandidateRepository
    {
        IUnitOfWork _uow;
        readonly IDbSet<Candidate> _Candidate;

        public CandidateRepository(IUnitOfWork uow)
        {
            _uow = uow;
            _Candidate = uow.Set<ElectionWebServerBot.Models.DomainModel.Candidate>();
        }
        public long Add(Candidate entity, bool autoSave = true)
        {
            try
            {
                _Candidate.Add(entity);
                if (autoSave)
                    if (Convert.ToBoolean(_uow.SaveAllChanges()))
                        return entity.Id;
                    else
                        return -1;
                else return -1;
            }
            catch
            {
                return -1;
            }
        }

        public bool Update(Candidate entity, bool autoSave = true)
        {
            try
            {
                _Candidate.Attach(entity);
                _uow.MarkAsChanged<Candidate>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Candidate entity, bool autoSave = true)
        {
            try
            {
                _uow.MarkAsDeleted<Candidate>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id, bool autoSave = true)
        {
            try
            {
                var entity = _Candidate.Find(id);
                _uow.MarkAsDeleted<Candidate>(entity);
                if (autoSave)
                    return Convert.ToBoolean(_uow.SaveAllChanges());
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public Candidate Find(long id)
        {
            try
            {
                return _Candidate.Find(id);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<Candidate> Where(System.Linq.Expressions.Expression<Func<Candidate, bool>> predicate)
        {
            try
            {
                return _Candidate.Where(predicate);
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<Candidate> Select()
        {
            try
            {
                return _Candidate.AsQueryable();
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<Candidate, TResult>> selector)
        {
            try
            {
                return _Candidate.Select(selector);
            }
            catch
            {
                return null;
            }
        }

        public long GetLastIdentity()
        {
            try
            {
                if (_Candidate.Any())
                    return _Candidate.OrderByDescending(p => p.Id).First().Id;
                else
                    return 0;
            }
            catch
            {
                return -1;
            }
        }

        public int Save()
        {
            try
            {
                return _uow.SaveAllChanges();
            }
            catch
            {
                return -1;
            }
        }




    }
}