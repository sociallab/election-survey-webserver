﻿using System;
namespace ElectionWebServerBot.Models.Repositories
{
    public interface ICandidateRepository
    {
        long Add(ElectionWebServerBot.Models.DomainModel.Candidate entity, bool autoSave = true);
        bool Delete(ElectionWebServerBot.Models.DomainModel.Candidate entity, bool autoSave = true);
        bool Delete(int id, bool autoSave = true);
        ElectionWebServerBot.Models.DomainModel.Candidate Find(long id);
        long GetLastIdentity();
        int Save();
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.Candidate> Select();
        System.Linq.IQueryable<TResult> Select<TResult>(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Candidate, TResult>> selector);
        bool Update(ElectionWebServerBot.Models.DomainModel.Candidate entity, bool autoSave = true);
        System.Linq.IQueryable<ElectionWebServerBot.Models.DomainModel.Candidate> Where(System.Linq.Expressions.Expression<Func<ElectionWebServerBot.Models.DomainModel.Candidate, bool>> predicate);
    }
}
