﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.ViewModels.Account
{
    public class ChangePass
    {
        [Required(ErrorMessage = "کلمه عبور قبلی باید وارد شود", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور قبلی")]
        public string OldPass { get; set; }
        [Required(ErrorMessage = "کلمه عبور جدید باید وارد شود", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور جدید")]
        public string NewPass { get; set; }
    }
}