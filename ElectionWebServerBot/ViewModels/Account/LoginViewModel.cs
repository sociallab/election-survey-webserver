﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ViewModels.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "نام کاربری باید وارد شود", AllowEmptyStrings = false)]
        [Display(Name = "نام کاربری")]
       
        public string Email { get; set; }

        [Required(ErrorMessage = "کلمه عبور باید وارد شود", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [Display(Name = "مرا به یاد بسپار؟")]
        public bool RememberMe { get; set; }
    }
}