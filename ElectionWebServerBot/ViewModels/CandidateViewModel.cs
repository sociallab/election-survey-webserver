﻿using ElectionWebServerBot.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.ViewModels
{
    public class CandidateViewModel
    {
        public CandidateViewModel()
        {

        }
        public CandidateViewModel(Candidate candidate, List<ElectionWebServerBot.Models.DomainModel.State> states, List<ElectionWebServerBot.Models.DomainModel.City> city)
        {
            Candidate = candidate;
            States = states;
            City = city;
        }
         //Tuple<ElectionWebServerBot.Models.DomainModel.Candidate, List<ElectionWebServerBot.Models.DomainModel.State>, List<ElectionWebServerBot.Models.DomainModel.City>>
        public Candidate Candidate { get; set; }
        public List<ElectionWebServerBot.Models.DomainModel.Candidate> Candidates { get; set; }

        public List<ElectionWebServerBot.Models.DomainModel.State> States { get; set; }

        public  List<ElectionWebServerBot.Models.DomainModel.City> City { get; set; }
    }
}