﻿using ElectionWebServerBot.Models.DomainModel;
using ElectionWebServerBot.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
namespace ElectionWebServerBot
{//updateItem.Message.MessageIdupdateItem.Message.MessageId
    public class MvcApplication : System.Web.HttpApplication
    {



        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
            ElectionWebServerBot.Jobs.YourJob.SomeScheduler.Start();
            //UnityConfig.RegisterComponents();
            //var unityContainer = Infrastructure.Bootstrapper.Initialise();
            //unityContainer.Resolve<ElectionWebServerBot.Jobs.IQuartzScheduler>().Run();
           
        }


    }
}
