$(function () {
  	$('.navbar-toggle-sidebar').click(function () {
  		$('.navbar-nav').toggleClass('slide-in');
  		$('.side-body').toggleClass('body-slide-in');
  		$('#search').removeClass('in').addClass('collapse').slideUp(200);
  	});

  	$('#search-trigger').click(function () {
  		$('.navbar-nav').removeClass('slide-in');
  		$('.side-body').removeClass('body-slide-in');
  		$('.search-input').focus();
  	});
  });
  
  

//Check All
$(document).ready(function () {
    $(document).on("click", "#mytable #checkall", function (e) {
        if ($("#mytable #checkall").is(':checked')) {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", true);


                var id = $(this).val()


                var itemExists = false;


                $("#Select1 option").each(function () {
                    if ($(this).val() == $.trim(id)) {
                        itemExists = true;
                    }
                });



                if (!itemExists && id != "on") {

                    $("#Select1").append("<option value='" + id + "'>" + $("#rntd" + id).text() + " : " + $("#rnftd" + id).text() + "</option>");
                    //////////////////////////////////RDODelivered
                    $("#Select1 option[value='" + id + "']").attr("hiddenSedorDate", $("#hiddenSedorDate" + id).val());
                    $("#Select1 option[value='" + id + "']").attr("hiddenFillableReciveDate", $("#hiddenFillableReciveDate" + id).val());
                    $("#Select1 option[value='" + id + "']").attr("hiddenFillableTahvilDate", $("#hiddenFillableTahvilDate" + id).val());
                    $("#Select1 option[value='" + id + "']").attr("hiddenTahvilGirande", $("#hiddenTahvilGirande" + id).val());
                    //////////////////////////////////RDODelivered

                    //////////////////////////////////RDOBack

                    $("#Select1 option[value='" + id + "']").attr("hiddenEditRejectDate", $("#hiddenEditRejectDate" + id).val());
                    $("#Select1 option[value='" + id + "']").attr("hiddenEditRejectReason", $("#hiddenEditRejectReason" + id).val());
                    //////////////////////////////////RDOBack

                    //////////////////////////////////RDOReadyFroDeliver
                    $("#Select1 option[value='" + id + "']").attr("hiddenSedorDate", $("#hiddenSedorDate" + id).val());
                    $("#Select1 option[value='" + id + "']").attr("hiddenNewSeri", $("#hiddenNewSeri" + id).val());
                    $("#Select1 option[value='" + id + "']").attr("hiddenNewSerial", $("#hiddenNewSerial" + id).val());
                    //////////////////////////////////RDOReadyFroDeliver
                }

            });

        } else {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", false);





                var id = $(this).val()
                var itemExists = false;


                $("#Select1 option").each(function () {
                    if ($(this).val() == $.trim(id)) {
                        itemExists = true;
                    }
                });
                if (itemExists) {
                    $("#Select1 option[value='" + id + "']").remove();
                }

            });
        }
    });

    $("[data-toggle=tooltip]").tooltip();
});


/////////////////////
function checkCodeMeli(code) {

    var L = code.length;

    if (L < 8 || parseInt(code, 10) == 0) return false;
    code = ('0000' + code).substr(L + 4 - 10);
    if (parseInt(code.substr(3, 6), 10) == 0) return false;
    var c = parseInt(code.substr(9, 1), 10);
    var s = 0;
    for (var i = 0; i < 9; i++)
        s += parseInt(code.substr(i, 1), 10) * (10 - i);
    s = s % 11;
    return (s < 2 && c == s) || (s >= 2 && c == (11 - s));
    return true;
}