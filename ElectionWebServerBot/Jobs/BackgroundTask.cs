﻿using ElectionWebServerBot;
using ElectionWebServerBot.Models.DomainModel;
using ElectionWebServerBot.Models.Repositories;
using Microsoft.Practices.Unity;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace ElectionWebServerBot.Jobs
{

    public class YourJob : IJob
    {

        public class Responses
        {
            readonly IUnitOfWork _uow;
            IRoleRepository repRoles;
            ISatrapInRoleRepository repUR;
            ICandidateRepository RepCandidate;
            ISatrapRepository RepSatrap;
            IStateRepository RepState;
            ICityRepository RepCity;
            IUserRepository RepUsers;
            IBotStateRepository RepBotStates;
            public Responses()
            {
                var unityContainer = new UnityContainer();

                unityContainer.RegisterType<IUnitOfWork, ElectionBotEntities1>();
                unityContainer.RegisterType<IRoleRepository, RoleRepository>();
                unityContainer.RegisterType<ISatrapInRoleRepository, SatrapInRoleRepository>();
                unityContainer.RegisterType<ICandidateRepository, CandidateRepository>();
                unityContainer.RegisterType<ISatrapRepository, SatrapRepository>();
                unityContainer.RegisterType<IStateRepository, StateRepository>();
                unityContainer.RegisterType<IUserRepository, UserRepository>();
                unityContainer.RegisterType<IBotStateRepository, BotStateRepository>();
                unityContainer.RegisterType<ICityRepository, CityRepository>();

                repRoles = unityContainer.Resolve<IRoleRepository>(); ;
                repUR = unityContainer.Resolve<ISatrapInRoleRepository>(); ;
                RepCandidate = unityContainer.Resolve<ICandidateRepository>(); ;
                RepSatrap = unityContainer.Resolve<ISatrapRepository>(); ;
                RepState = unityContainer.Resolve<IStateRepository>(); ;
                RepCity = unityContainer.Resolve<ICityRepository>(); ;
                RepUsers = unityContainer.Resolve<IUserRepository>(); ;
                RepBotStates = unityContainer.Resolve<IBotStateRepository>(); ;
            }
            Semaphore sem = new Semaphore(1, 1);
            private string[][] GetMarkupKeyboardStates(bool isBackButton = false)
            {
                try
                {
                    var CandidateStates = RepCandidate.Select(x => x.StateID).Distinct().ToList();
                    string[][] KeyArray = (isBackButton) ? KeyArray =
                        new string[Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(CandidateStates.Count()) / 3))) + 1][] :
                     KeyArray = new string[Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(CandidateStates.Count()) / 3)))][];




                    int counter = 0;
                    int MainCounter = 0;
                    var TmpStringArray = new string[3];
                    for (int i = 0; i < CandidateStates.Count; i++)
                    {
                        if (counter == 0) TmpStringArray = new string[3] { "-", "-", "-" };
                        var state = RepState.Find(CandidateStates[i]);
                        string stateName = "-";
                        if (state != null)
                        {
                            stateName = state.Title;
                        }
                        TmpStringArray[counter++] = stateName;


                        if (counter == 3 || i == CandidateStates.Count - 1)
                        {
                            KeyArray[MainCounter++] = TmpStringArray;
                            counter = 0;
                        }


                    }

                    if (isBackButton)
                    {
                        KeyArray[KeyArray.Count() - 1] = new string[] { "برگشت" };
                    }
                    return KeyArray;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            private string[][] GetMarkupKeyboardCities(byte stateId, bool isBackButton = false)
            {
                try
                {
                    var CandidateCitiesInState = RepCandidate.Where(x => x.StateID == stateId).Select(x => x.CityId);
                    var StateCities = RepCity.Where(x => x.StateId == stateId).ToList();
                    var CandidatesCities = (from x in StateCities
                                            join y in CandidateCitiesInState
                                            on x.CityId equals y
                                            select x
                                         ).Distinct().ToList();
                    string[][] KeyArry = (isBackButton) ? KeyArry =
                        new string[Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(CandidatesCities.Count()) / 3))) + 1][] :
                     KeyArry = new string[Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(CandidatesCities.Count()) / 3)))][];
                    int counter = 0;
                    int MainCounter = 0;
                    var TmpStringArry = new string[3];
                    for (int i = 0; i < CandidatesCities.Count; i++)
                    {
                        if (counter == 0) TmpStringArry = new string[3] { "-", "-", "-" };
                        var state = CandidatesCities[i];
                        string CityName = "-";
                        if (state != null)
                        {
                            CityName = state.Title;
                        }
                        TmpStringArry[counter++] = CityName;


                        if (counter == 3 || i == CandidatesCities.Count - 1)
                        {
                            KeyArry[MainCounter++] = TmpStringArry;
                            counter = 0;
                        }


                    }
                    if (isBackButton)
                    {
                        KeyArry[KeyArry.Count() - 1] = new string[] { "برگشت" };
                    }
                    return KeyArry;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            private string[][] GetMarkupKeyboardCandidates(byte stateId, int cityId, bool isBackButton = false)
            {
                try
                {
                    var Candidates = RepCandidate.Where(x => x.StateID == stateId && x.CityId == cityId && x.Enable).Distinct().ToList();

                    var KeyArry = new string[Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(Candidates.Count()) / 3))) + 1][];
                    int counter = 0;
                    int MainCounter = 0;
                    var TmpStringArry = new string[3];
                    for (int i = 0; i < Candidates.Count; i++)
                    {
                        if (counter == 0) TmpStringArry = new string[3] { "-", "-", "-" };
                        var Candidate = Candidates[i];
                        string CandidateName = "-";
                        if (Candidate != null)
                        {
                            CandidateName = Candidate.FullName;
                        }
                        TmpStringArry[counter++] = CandidateName;


                        if (counter == 3 || i == Candidates.Count - 1)
                        {
                            KeyArry[MainCounter++] = TmpStringArry;
                            counter = 0;
                        }


                    }

                    if (isBackButton)
                    {
                        KeyArry[KeyArry.Count() - 1] = new string[] { "برگشت" };
                    }
                    else
                    {
                        KeyArry[KeyArry.Count() - 1] = new string[1] { "هیچ کدام" };
                    }
                    return KeyArry;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            private string[][] GetMarkupKeyboardMainMenu(byte MenuType = 0)
            {
                switch (MenuType)
                {
                    case 0:
                        return new string[][] { new string[] {"نام و نام خانوادگی", "درباره", "عکس", "تحصیلات" },
                    new string[] { "پیام", "حزب سیاسی", "اطلاعات تماس کاندید" },
                   
                new string[]{"جستجو کاندید ها","ویرایش کاندید منتخب شما"},
                new string[]{"سفارش ربات","اضافه کردن کاندید","مدیریت پنل کاندید"}
                };
                    case 2:
                        return new string[][] { new string[] { "نام و نام خانوادگی","درباره", "عکس", "تحصیلات" },
                    new string[] { "پیام", "حزب سیاسی", "اطلاعات تماس کاندید" },new string[]{"منوی اصلی"},new string[]{"برگشت"},};
                    default:
                        return new string[][] { new string[] {"نام و نام خانوادگی", "درباره", "عکس", "تحصیلات" },
                    new string[] { "پیام", "حزب سیاسی", "اطلاعات تماس کاندید" },
            };


                }
            }
            public async void ResponseToUsers(Telegram.Bot.Types.Update updateItem)
            {
                // return;
                try
                {
                    var user = RepUsers.Where(x => x.Chat_Id == updateItem.Message.From.Id).FirstOrDefault();


                    if (user == null)
                    {
                        sem.WaitOne();
                        user = RepUsers.Where(x => x.Chat_Id == updateItem.Message.From.Id).FirstOrDefault();
                        if (user == null)
                        {
                            try
                            {
                                await Statics.Bot.SendTextMessage(updateItem.Message.From.Id, "تذکر: مشخصات (به جزء  نام، نام خانوادگی، تحصیلات، عکس )  توسط کاندیدا ها و اشخاص نزدیک به ایشان در بانک اطلاعاتی ثبت گردیده است و مسئولیت تخلف از این اطلاعات( از جمله تخلفات تبلیغاتی خارج از موعد قانونی) بر عهده آنها می باشد.");

                            }
                            catch (Exception)
                            {

                            }


                            var newUser = new ElectionWebServerBot.Models.DomainModel.User
                            {
                                Chat_Id = updateItem.Message.From.Id,
                                CurrentState = "Start",
                            };

                            try
                            {
                                newUser.FirstName = updateItem.Message.From.FirstName;
                            }
                            catch
                            {
                            }
                            try
                            {
                                newUser.LastName = updateItem.Message.From.LastName;
                            }
                            catch
                            {
                            }
                            try
                            {
                                newUser.UserName = updateItem.Message.From.Username;
                            }
                            catch
                            {
                            }


                            var UserId = RepUsers.Add(newUser);

                            if (UserId != -1)
                            {// User registered


                                var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                mr.ResizeKeyboard = true;
                                mr.Keyboard = GetMarkupKeyboardStates();
                                newUser.CurrentState = "Start_GetCity";
                                if (RepUsers.Update(newUser))
                                {
                                    try
                                    {
                                        var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "یک استان جهت انتخاب کاندید مورد نظرتان انتخاب کنید", false, 0, mr);
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }

                            }
                            else
                            {//User did not register
                                var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                try
                                {
                                    mr.Keyboard = new string[][] { new string[] { "شروع" } };
                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای شروع مجدد بر روی دکمه شروع کلیک کنید.", false, 0, mr);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                        sem.Release();
                    }
                    else
                    {
                        try
                        {
                            switch (user.CurrentState)
                            {
                                case "Start":
                                    {

                                        using (var dbContextTransaction = new TransactionScope())
                                        {
                                            try
                                            {
                                                var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                mr.ResizeKeyboard = true;
                                                mr.Keyboard = GetMarkupKeyboardStates();
                                                user.CurrentState = "Start_GetCity";
                                                if (RepUsers.Update(user))
                                                {
                                                    try
                                                    {
                                                        var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "یک استان جهت انتخاب کاندید مورد نظرتان انتخاب کنید", false, 0, mr);
                                                        if (sRes.Text.Contains("استان")) dbContextTransaction.Complete();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                }
                                                else
                                                {
                                                    try
                                                    {

                                                        var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                                    }
                                                    catch
                                                    {


                                                    }


                                                }
                                            }
                                            catch (Exception)
                                            {


                                            }
                                        }
                                        break;
                                    }
                                case "Start_GetCity":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;

                                        var SelectedState = RepState.Where(x => x.Title == updateItem.Message.Text).FirstOrDefault();
                                        if (SelectedState == null)
                                        {
                                            try
                                            {
                                                mr.Keyboard = GetMarkupKeyboardStates();
                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه چنین استانی در پایگاه داده ثبت نشده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید", false, 0, mr);
                                            }
                                            catch
                                            {


                                            }

                                        }
                                        else
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {
                                                    user.Temp_State_ID = SelectedState.Id;
                                                    user.CurrentState = "Start_GetCandidate";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardCities(SelectedState.Id);
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "شهر مورد نظرتان انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("شهر"))
                                                                dbContextTransaction.Complete();

                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }


                                        }

                                        break;
                                    }
                                case "Start_GetCandidate":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        byte StateId = user.Temp_State_ID ?? 16;
                                        var SelectedCity = RepCity.Where(x => x.StateId == user.Temp_State_ID).Where(x => x.Title == updateItem.Message.Text).FirstOrDefault();
                                        if (SelectedCity == null)
                                        {
                                            try
                                            {
                                                mr.Keyboard = GetMarkupKeyboardCities(StateId);
                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه چنین شهری در پایگاه داده ثبت نشده است. برای ادامه مجدداً شهر مورد نظرتان را انتخاب کنید", false, 0, mr);
                                            }
                                            catch
                                            {
                                            }

                                        }
                                        else
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {
                                                    user.Temp_City_Id = SelectedCity.CityId;
                                                    user.CurrentState = "Start_PostCandidate";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardCandidates(StateId, SelectedCity.CityId);
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "کاندید منتخبتان را انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("کاندید"))
                                                                dbContextTransaction.Complete();


                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }


                                        }

                                        break;
                                    }
                                case "Start_PostCandidate":
                                    {

                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        byte StateId = user.Temp_State_ID ?? 16;
                                        if (updateItem.Message.Text == "هیچ کدام")
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {

                                                    user.CurrentState = "MainMenu";

                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "شما می توانید با انتخاب دکمه (انتخاب / ویرایش کاندید منتخب) از کاندید مورد نظرتان حمایت کنید و یا با استفاده از دکمه (جستجو کردن کاندید ها) با کاندید ها آشنا شوید", false, 0, mr);
                                                            if (sRes.Text.Contains("آشنا"))
                                                                dbContextTransaction.Complete();
                                                            else
                                                            {

                                                            }
                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            // mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نماید.", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }

                                        }
                                        else
                                        {
                                            var SelectedCandidate = RepCandidate.Where(x => x.StateID == user.Temp_State_ID && x.CityId == user.Temp_City_Id && x.FullName == updateItem.Message.Text).FirstOrDefault();


                                            if (SelectedCandidate == null)
                                            {
                                                try
                                                {
                                                    mr.Keyboard = GetMarkupKeyboardCandidates(StateId, user.Temp_City_Id ?? 22);
                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه کاندیدی با این مشخصات پیدا نشد", false, 0, mr);
                                                }
                                                catch
                                                {
                                                }

                                            }
                                            else
                                            {
                                                using (var dbContextTransaction = new TransactionScope())
                                                {
                                                    try
                                                    {
                                                        user.CandidateId = SelectedCandidate.Id;
                                                        user.CurrentState = "MainMenu";
                                                        if (RepUsers.Update(user))
                                                        {
                                                            mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                            try
                                                            {
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "عملیات با موفقیت انجام شد. برای ویرایش کاندید منتخب از دمکمه (انتخاب / ویرایش کاندید منتخب) استفاد کنید . در ضمن به غیر کاندید منتخبتان شما می تواند با استفاده از دکمه (جستجو کردن کاندید ها) با کاندید های دیگر استان و شهر ها هم آشنا شوید", false, 0, mr);
                                                                if (sRes.Text.Contains("شهر"))
                                                                    dbContextTransaction.Complete();

                                                            }
                                                            catch
                                                            {


                                                            }
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {

                                                                //mr.Keyboard = GetMarkupKeyboardCandidates();
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً کاندید مورد نظرتان را انتخاب کنید.", false, 0, mr);
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }

                                                        }



                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                }


                                            }
                                        }
                                        break;
                                    }
                                case "MainMenu":
                                    {
                                        //if (user.CandidateId == null)
                                        //{
                                        //    try
                                        //    {
                                        //        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                        //        return;
                                        //    }
                                        //    catch (Exception)
                                        //    {


                                        //    }
                                        //}
                                        Candidate UserCandidate = new Candidate();
                                        if (user.CandidateId != null)
                                        {
                                            UserCandidate = RepCandidate.Where(x => x.Id == user.CandidateId).FirstOrDefault();

                                        }
                                        switch (updateItem.Message.Text)
                                        {

                                            case "سفارش ربات":
                                                {
                                                    try
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "برای سفارش ربات درخواست خود را به @Kiaksarg یا +989398035241 بفرستید.");
                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                    break;
                                                }
                                            case "اضافه کردن کاندید":
                                                {
                                                    try
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "برای اضافه کردن کاندید درخواست خود را به @Kiaksarg یا +989398035241 بفرستید.");
                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                    break;
                                                }
                                            case "مدیریت پنل کاندید":
                                                {
                                                    try
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "هر کاندید می تواند اطلاعات خود را مدیریت کند .برای مدیریت پنل کاندید درخواست خود را به @Kiaksarg یا +989398035241 بفرستید. ");
                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                    break;
                                                }
                                            //
                                            case "نام و نام خانوادگی":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        try
                                                        {
                                                            await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");

                                                        }
                                                        catch (Exception)
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.FullName) || string.IsNullOrWhiteSpace(UserCandidate.FullName))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز متنی در این بخش برای این کاندید وارد نشده است.");

                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserCandidate.FullName);

                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }

                                            case "عکس":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.Pic) || string.IsNullOrWhiteSpace(UserCandidate.Pic))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز عکسی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "در حال آپلود عکس...");

                                                                Telegram.Bot.Types.FileToSend Photo = new Telegram.Bot.Types.FileToSend();
                                                                Photo.Filename = System.Web.Hosting.HostingEnvironment.MapPath("~/") + "CandidateFiles\\Pics\\" + UserCandidate.Pic;
                                                                Photo.Content = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath("~/") + "CandidateFiles\\Pics\\" + UserCandidate.Pic, FileMode.Open);
                                                                await Statics.Bot.SendPhoto(updateItem.Message.Chat.Id, Photo);

                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "درباره":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.Description) || string.IsNullOrWhiteSpace(UserCandidate.Description))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز متنی در این بخش برای این کاندید وارد نشده است.");

                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserCandidate.Description);

                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "تحصیلات":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.Education) || string.IsNullOrWhiteSpace(UserCandidate.Education))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserCandidate.Education);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "پیام":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.Message) || string.IsNullOrWhiteSpace(UserCandidate.Message))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserCandidate.Message);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "حزب سیاسی":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.Party) || string.IsNullOrWhiteSpace(UserCandidate.Party))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserCandidate.Party);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "اطلاعات تماس کاندید":
                                                {
                                                    if (user.CandidateId == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserCandidate.ContactInfos) || string.IsNullOrWhiteSpace(UserCandidate.ContactInfos))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserCandidate.ContactInfos);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "جستجو کاندید ها":
                                                {
                                                    using (var dbContextTransaction = new TransactionScope())
                                                    {
                                                        try
                                                        {
                                                            var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                            mr.ResizeKeyboard = true;
                                                            mr.Keyboard = GetMarkupKeyboardStates(true);
                                                            user.CurrentState = "Search_GetState";
                                                            if (RepUsers.Update(user))
                                                            {
                                                                try
                                                                {
                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "یک استان جهت انتخاب کاندید مورد نظرتان انتخاب کنید", false, 0, mr);
                                                                    if (sRes.Text.Contains("استان")) dbContextTransaction.Complete();
                                                                }
                                                                catch (Exception)
                                                                {


                                                                }

                                                            }
                                                            else
                                                            {
                                                                try
                                                                {

                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                                                }
                                                                catch
                                                                {


                                                                }


                                                            }
                                                        }
                                                        catch (Exception)
                                                        {


                                                        }
                                                    }
                                                    break;

                                                }

                                            case "ویرایش کاندید منتخب شما":
                                                {
                                                    using (var dbContextTransaction = new TransactionScope())
                                                    {
                                                        try
                                                        {
                                                            var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                            mr.ResizeKeyboard = true;
                                                            mr.Keyboard = GetMarkupKeyboardStates(true);
                                                            user.CurrentState = "Edit_Candidate_GetCity";
                                                            if (RepUsers.Update(user))
                                                            {
                                                                try
                                                                {
                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "یک استان جهت انتخاب کاندید مورد نظر انتخاب کنید", false, 0, mr);
                                                                    if (sRes.Text.Contains("استان")) dbContextTransaction.Complete();
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }

                                                            }
                                                            else
                                                            {
                                                                try
                                                                {

                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید.", false, 0, mr);
                                                                }
                                                                catch
                                                                {


                                                                }

                                                            }
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                    }
                                                    break;

                                                }
                                            default:
                                                {
                                                    var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                    mr.ResizeKeyboard = true;
                                                    mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه پاسخی برای درخواست شما یافت نشد...!", false, 0, mr);
                                                    break;
                                                }
                                        }

                                        break;
                                    }
                                case "Search_GetState":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        if (updateItem.Message.Text == "برگشت")
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {
                                                    user.CurrentState = "MainMenu";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "برگشت به منو...", false, 0, mr);
                                                            if (sRes.Text.Contains("برگشت"))
                                                                dbContextTransaction.Complete();

                                                        }
                                                        catch
                                                        {
                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            //mr.Keyboard = GetMarkupKeyboardCandidates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نمایید.", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }

                                        }
                                        else
                                        {
                                            var SelectedState = RepState.Where(x => x.Title == updateItem.Message.Text).FirstOrDefault();
                                            if (SelectedState == null)
                                            {
                                                try
                                                {
                                                    // mr.Keyboard = GetMarkupKeyboardStates();
                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه چنین استانی در پایگاه داده ثبت نشده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید");
                                                }
                                                catch
                                                {


                                                }

                                            }
                                            else
                                            {
                                                using (var dbContextTransaction = new TransactionScope())
                                                {
                                                    try
                                                    {
                                                        user.Searched_State_Id = SelectedState.Id;
                                                        user.CurrentState = "Search_GetCity";
                                                        if (RepUsers.Update(user))
                                                        {
                                                            mr.Keyboard = GetMarkupKeyboardCities(SelectedState.Id, true);
                                                            try
                                                            {
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "شهر مورد نظرتان انتخاب کنید", false, 0, mr);
                                                                if (sRes.Text.Contains("شهر"))
                                                                    dbContextTransaction.Complete();
                                                            }
                                                            catch
                                                            {
                                                            }
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {

                                                                //mr.Keyboard = GetMarkupKeyboardStates();
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..");
                                                            }
                                                            catch (Exception)
                                                            {
                                                            }

                                                        }



                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                }


                                            }
                                        }


                                        break;
                                    }
                                case "Search_GetCity":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        if (updateItem.Message.Text == "برگشت")
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {

                                                    mr.Keyboard = GetMarkupKeyboardStates(true);
                                                    user.CurrentState = "Search_GetState";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "یک استان جهت انتخاب کاندید مورد نظرتان انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("استان")) dbContextTransaction.Complete();
                                                        }
                                                        catch (Exception)
                                                        {


                                                        }

                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نمایید.", false, 0, mr);
                                                        }
                                                        catch
                                                        {


                                                        }


                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }
                                            break;
                                        }
                                        byte StateId = user.Searched_State_Id ?? 16;
                                        var SelectedCity = RepCity.Where(x => x.StateId == user.Searched_State_Id).Where(x => x.Title == updateItem.Message.Text).FirstOrDefault();
                                        if (SelectedCity == null)
                                        {
                                            try
                                            {
                                                // mr.Keyboard = GetMarkupKeyboardCities(StateId,);
                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه چنین شهری در پایگاه داده ثبت نشده است. برای ادامه مجدداً شهر مورد نظرتان را انتخاب کنید");
                                            }
                                            catch
                                            {
                                            }

                                        }
                                        else
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {
                                                    user.Searched_City_Id = SelectedCity.CityId;
                                                    user.CurrentState = "Search_Candidate";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardCandidates(StateId, SelectedCity.CityId, true);
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "کاندید مورد نظر را انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("کاندید"))
                                                                dbContextTransaction.Complete();


                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            // mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }


                                        }

                                        break;
                                    }
                                case "Search_Candidate":
                                    {

                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;

                                        if (updateItem.Message.Text == "برگشت")
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {

                                                    user.CurrentState = "Search_GetCity";

                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardCities(user.Searched_State_Id ?? Convert.ToByte(16), true);
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "شهر را انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("انتخاب"))
                                                                dbContextTransaction.Complete();
                                                            else
                                                            {

                                                            }
                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            // mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نماید.", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }

                                        }
                                        else
                                        {
                                            var SelectedCandidate = RepCandidate.Where(x => x.StateID == user.Searched_State_Id && x.CityId == user.Searched_City_Id && x.FullName == updateItem.Message.Text).FirstOrDefault();


                                            if (SelectedCandidate == null)
                                            {
                                                try
                                                {
                                                    // mr.Keyboard = GetMarkupKeyboardCandidates(user.Searched_State_Id ?? Convert.ToByte(16), user.Searched_City_Id ?? 22);
                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه کاندیدی با این مشخصات پیدا نشد");
                                                }
                                                catch
                                                {
                                                }

                                            }
                                            else
                                            {
                                                using (var dbContextTransaction = new TransactionScope())
                                                {
                                                    try
                                                    {
                                                        user.Searched_Candidate = SelectedCandidate.Id;
                                                        user.CurrentState = "Search_MainMenu";
                                                        if (RepUsers.Update(user))
                                                        {
                                                            mr.Keyboard = GetMarkupKeyboardMainMenu(2);
                                                            try
                                                            {
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, updateItem.Message.Text + " انتخاب شد", false, 0, mr);
                                                                if (sRes.Text.Contains("انتخاب"))
                                                                    dbContextTransaction.Complete();

                                                            }
                                                            catch
                                                            {


                                                            }
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {

                                                                //mr.Keyboard = GetMarkupKeyboardCandidates();
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً کاندید مورد نظرتان را انتخاب کنید.", false, 0, mr);
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }

                                                        }



                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                }


                                            }
                                        }
                                        break;
                                    }
                                case "Search_MainMenu":
                                    {
                                        if (user.Searched_Candidate == null)
                                        {
                                            try
                                            {
                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید منتخبتان را مشخص کنید...!");
                                                return;
                                            }
                                            catch (Exception)
                                            {


                                            }
                                        }
                                        var UserSearchedCandidate = RepCandidate.Where(x => x.Id == user.Searched_Candidate).FirstOrDefault();
                                        switch (updateItem.Message.Text)
                                        {
                                            case "نام و نام خانوادگی":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.FullName) || string.IsNullOrWhiteSpace(UserSearchedCandidate.FullName))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز متنی در این بخش برای این کاندید وارد نشده است.");

                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserSearchedCandidate.FullName);

                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "عکس":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.Pic) || string.IsNullOrWhiteSpace(UserSearchedCandidate.Pic))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز عکسی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "در حال آپلود عکس...");

                                                                Telegram.Bot.Types.FileToSend Photo = new Telegram.Bot.Types.FileToSend();
                                                                Photo.Filename = System.Web.Hosting.HostingEnvironment.MapPath("~/") + "CandidateFiles\\Pics\\" + UserSearchedCandidate.Pic;
                                                                Photo.Content = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath("~/") + "CandidateFiles\\Pics\\" + UserSearchedCandidate.Pic, FileMode.Open);
                                                                await Statics.Bot.SendPhoto(updateItem.Message.Chat.Id, Photo);

                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "درباره":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.Description) || string.IsNullOrWhiteSpace(UserSearchedCandidate.Description))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز متنی در این بخش برای این کاندید وارد نشده است.");

                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserSearchedCandidate.Description);

                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "تحصیلات":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.Education) || string.IsNullOrWhiteSpace(UserSearchedCandidate.Education))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserSearchedCandidate.Education);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "پیام":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.Message) || string.IsNullOrWhiteSpace(UserSearchedCandidate.Message))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserSearchedCandidate.Message);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "حزب سیاسی":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.Party) || string.IsNullOrWhiteSpace(UserSearchedCandidate.Party))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserSearchedCandidate.Party);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "اطلاعات تماس کاندید":
                                                {
                                                    if (user.Searched_Candidate == null)
                                                    {
                                                        await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "اول باید کاندید مورد نظر را مشخص کنید...!");
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (string.IsNullOrEmpty(UserSearchedCandidate.ContactInfos) || string.IsNullOrWhiteSpace(UserSearchedCandidate.ContactInfos))
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, " هنوز اطلاعاتی در این بخش برای این کاندید وارد نشده است.");
                                                            }
                                                            else
                                                            {
                                                                await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, UserSearchedCandidate.ContactInfos);
                                                            }

                                                        }
                                                        catch (Exception)
                                                        {
                                                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");

                                                        }
                                                    }


                                                    break;
                                                }
                                            case "برگشت":
                                                {
                                                    using (var dbContextTransaction = new TransactionScope())
                                                    {
                                                        try
                                                        {
                                                            var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                            mr.ResizeKeyboard = true;
                                                            user.CurrentState = "Search_Candidate";
                                                            if (RepUsers.Update(user))
                                                            {
                                                                mr.Keyboard = GetMarkupKeyboardCandidates(user.Searched_State_Id ?? 16, user.Searched_City_Id ?? 22, true);
                                                                try
                                                                {
                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "کاندید مورد نظر را انتخاب کنید", false, 0, mr);
                                                                    if (sRes.Text.Contains("کاندید"))
                                                                        dbContextTransaction.Complete();


                                                                }
                                                                catch
                                                                {


                                                                }
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {

                                                                    // mr.Keyboard = GetMarkupKeyboardStates();
                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است.", false);
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }

                                                            }



                                                        }
                                                        catch (Exception)
                                                        {


                                                        }
                                                    }

                                                    break;

                                                }
                                            case "منوی اصلی":
                                                {
                                                    var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                    mr.ResizeKeyboard = true;
                                                    using (var dbContextTransaction = new TransactionScope())
                                                    {
                                                        try
                                                        {

                                                            user.CurrentState = "MainMenu";
                                                            if (RepUsers.Update(user))
                                                            {
                                                                mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                                try
                                                                {
                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "برگشت به منو...", false, 0, mr);
                                                                    if (sRes.Text.Contains("برگشت"))
                                                                        dbContextTransaction.Complete();

                                                                }
                                                                catch
                                                                {

                                                                }
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {

                                                                    //mr.Keyboard = GetMarkupKeyboardCandidates();
                                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نمایید.", false, 0, mr);
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }

                                                            }



                                                        }
                                                        catch (Exception)
                                                        {


                                                        }
                                                    }
                                                    break;
                                                }
                                            default:
                                                {
                                                    var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                                    mr.ResizeKeyboard = true;
                                                    mr.Keyboard = GetMarkupKeyboardMainMenu(2);
                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه پاسخی برای درخواست شما یافت نشد...!", false, 0, mr);
                                                    break;
                                                }
                                        }

                                        break;
                                    }
                                case "Edit_Candidate_GetCity":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        if (updateItem.Message.Text == "برگشت")
                                        {

                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {

                                                    user.CurrentState = "MainMenu";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "برگشت به منو...", false, 0, mr);
                                                            if (sRes.Text.Contains("برگشت"))
                                                                dbContextTransaction.Complete();

                                                        }
                                                        catch
                                                        {

                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            //mr.Keyboard = GetMarkupKeyboardCandidates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نمایید.", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }
                                            break;
                                        }
                                        var SelectedState = RepState.Where(x => x.Title == updateItem.Message.Text).FirstOrDefault();
                                        if (SelectedState == null)
                                        {
                                            try
                                            {

                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه چنین استانی در پایگاه داده ثبت نشده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید");
                                            }
                                            catch
                                            {


                                            }

                                        }
                                        else
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {
                                                    user.Temp_State_ID = SelectedState.Id;
                                                    user.CurrentState = "Edit_Candidate_GetCandidate";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardCities(SelectedState.Id);
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "شهر مورد نظرتان انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("شهر"))
                                                                dbContextTransaction.Complete();

                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }


                                        }

                                        break;
                                    }
                                case "Edit_Candidate_GetCandidate":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        byte StateId = user.Temp_State_ID ?? 16;
                                        var SelectedCity = RepCity.Where(x => x.StateId == user.Temp_State_ID).Where(x => x.Title == updateItem.Message.Text).FirstOrDefault();
                                        if (SelectedCity == null)
                                        {
                                            try
                                            {
                                                mr.Keyboard = GetMarkupKeyboardCities(StateId);
                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه چنین شهری در پایگاه داده ثبت نشده است. برای ادامه مجدداً شهر مورد نظرتان را انتخاب کنید", false, 0, mr);
                                            }
                                            catch
                                            {
                                            }

                                        }
                                        else
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {
                                                    user.Temp_City_Id = SelectedCity.CityId;
                                                    user.CurrentState = "Edit_Candidate_PostCandidate";
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardCandidates(StateId, SelectedCity.CityId);
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "کاندید منتخبتان را انتخاب کنید", false, 0, mr);
                                                            if (sRes.Text.Contains("کاندید"))
                                                                dbContextTransaction.Complete();


                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما خطا رخ داده است. برای ادامه مجدداً استان مورد نظرتان را انتخاب کنید..", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }


                                        }

                                        break;
                                    }
                                case "Edit_Candidate_PostCandidate":
                                    {
                                        var mr = new Telegram.Bot.Types.ReplyKeyboardMarkup() { };
                                        mr.ResizeKeyboard = true;
                                        byte StateId = user.Temp_State_ID ?? 16;
                                        if (updateItem.Message.Text == "هیچ کدام")
                                        {
                                            using (var dbContextTransaction = new TransactionScope())
                                            {
                                                try
                                                {

                                                    user.CurrentState = "MainMenu";
                                                    user.CandidateId = null;
                                                    if (RepUsers.Update(user))
                                                    {
                                                        mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                        try
                                                        {
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "شما از لیست هواداران کاندید منتخبتان حذف شدید برای انتخاب یک کاندید به عنوان کاندید منتخب مجددا از دکمه ویرایش استفاده کنید.", false, 0, mr);
                                                            if (sRes.Text.Contains("اصلی"))
                                                                dbContextTransaction.Complete();
                                                            else
                                                            {

                                                            }
                                                        }
                                                        catch
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {

                                                            // mr.Keyboard = GetMarkupKeyboardStates();
                                                            var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً تلاش نماید.", false, 0, mr);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }



                                                }
                                                catch (Exception)
                                                {


                                                }
                                            }

                                        }
                                        else
                                        {
                                            var SelectedCandidate = RepCandidate.Where(x => x.StateID == user.Temp_State_ID && x.CityId == user.Temp_City_Id && x.FullName == updateItem.Message.Text).FirstOrDefault();


                                            if (SelectedCandidate == null)
                                            {
                                                try
                                                {
                                                    mr.Keyboard = GetMarkupKeyboardCandidates(StateId, user.Temp_City_Id ?? 22);
                                                    var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه کاندیدی با این مشخصات پیدا نشد", false, 0, mr);
                                                }
                                                catch
                                                {
                                                }

                                            }
                                            else
                                            {
                                                using (var dbContextTransaction = new TransactionScope())
                                                {
                                                    try
                                                    {
                                                        user.CandidateId = SelectedCandidate.Id;
                                                        user.CurrentState = "MainMenu";
                                                        if (RepUsers.Update(user))
                                                        {
                                                            mr.Keyboard = GetMarkupKeyboardMainMenu();
                                                            try
                                                            {
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "کاندید منتخب شما با موفقیت ویرایش شد", false, 0, mr);
                                                                if (sRes.Text.Contains("شهر"))
                                                                    dbContextTransaction.Complete();

                                                            }
                                                            catch
                                                            {


                                                            }
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {

                                                                //mr.Keyboard = GetMarkupKeyboardCandidates();
                                                                var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در ثبت اطلاعات شما مشکلس رخ داده است. برای ادامه مجدداً کاندید مورد نظرتان را انتخاب کنید.", false, 0, mr);
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }

                                                        }



                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                }


                                            }
                                        }
                                        break;
                                    }
                                default:
                                    {

                                        var sRes = await Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه پاسخی برای درخواست شما یافت نشد...!");

                                        break;
                                    }
                            }
                        }
                        catch (Exception)
                        {
                            Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است...!");
                            //err
                        }

                    }

                }
                catch
                {
                    Statics.Bot.SendTextMessage(updateItem.Message.Chat.Id, "متاسفانه در پردازش درخواست شما خطای رخ داده است ...!");
                }

            }

        }



        async void GetUpdates()
        {
            Thread.CurrentThread.IsBackground = false;

            while (true)
            {
                try
                {
                    var me = await Statics.Bot.GetUpdates(Statics.offset + 1);


                    foreach (var item in me)
                    {
                        Statics.offset = item.Id;

                        var resptou= new Responses();
                        new Task(() => resptou.ResponseToUsers(item)).Start();

                    }
                }
                catch
                {
                    //Thread.Sleep(1000);
                }


            }

        }
        public async void Execute(IJobExecutionContext context)
        {
            try
            {
                var me = await Statics.Bot.GetUpdates(Statics.offset + 1);


                foreach (var item in me)
                {
                    Statics.offset = item.Id;

                    var resptou = new Responses();
                    new Task(() => resptou.ResponseToUsers(item)).Start();
                }
            }
            catch
            {
                //Thread.Sleep(1000);
            }
        }

        public class SomeScheduler
        {
            public static void Start()
            {
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<YourJob>().Build();
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger1", "group1")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithInterval(TimeSpan.FromSeconds(1))
                        .RepeatForever())
                    .Build();

                scheduler.ScheduleJob(job, trigger);
            }
        }
        public class BackgroundTask
        {

        }

    }


}