﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Jobs
{
    public interface IQuartzScheduler
    {
        void Run();
        void Stop();
    }
}