﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectionWebServerBot.Jobs
{
    using Quartz;
    using Quartz.Impl;
    using Quartz.Impl.Triggers;

    public class QuartzScheduler : IQuartzScheduler
    {
        private readonly ISchedulerFactory SchedulerFactory;
        private readonly IScheduler Scheduler;

        public QuartzScheduler(ISchedulerFactory schedulerFactory, IScheduler scheduler)
        {
            this.SchedulerFactory = schedulerFactory;
            this.Scheduler = scheduler;
        }
        
        public void Run()
        {
            IJobDetail job = new JobDetailImpl("BotJob", null, typeof(ElectionWebServerBot.Jobs.YourJob));
            // fire every time I open App/EveryDay
            ITrigger trigger = TriggerBuilder.Create()
          .WithIdentity("Bot", "group1")
         // .StartNow()
          .WithSimpleSchedule(x => x              
              .RepeatForever())
          .Build();
            this.Scheduler.ScheduleJob(job, trigger);
            this.Scheduler.Start();
        }

        public void Stop()
        {
            this.Scheduler.Shutdown(false);
        }
    }
}