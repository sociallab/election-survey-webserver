using System.Web.Mvc;

namespace ElectionWebServerBot.Infrastructure
{
    using Quartz;
    using ElectionWebServerBot.Jobs;
    using Microsoft.Practices.Unity;
    using Unity.Mvc5;
    using Quartz.Core;
    using ElectionWebServerBot.Models.Repositories;
    using ElectionWebServerBot.Models.DomainModel;

    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<ISchedulerFactory, UnitySchedulerFactory>(new ContainerControlledLifetimeManager());
            container.RegisterType<IScheduler>(new InjectionFactory(c => c.Resolve<ISchedulerFactory>().GetScheduler()));
            container.RegisterType<IQuartzScheduler, ElectionWebServerBot.Jobs.QuartzScheduler>(new ContainerControlledLifetimeManager());

            container.RegisterType<IUnitOfWork, ElectionBotEntities1>();
            container.RegisterType<IRoleRepository, RoleRepository>();
            container.RegisterType<ISatrapInRoleRepository, SatrapInRoleRepository>();
            container.RegisterType<ICandidateRepository, CandidateRepository>();
            container.RegisterType<ISatrapRepository, SatrapRepository>();
            container.RegisterType<IStateRepository, StateRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IBotStateRepository, BotStateRepository>();
            container.RegisterType<ICityRepository, CityRepository>();

            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }
    }
}