﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StructureMap;
using Quartz;
using Quartz.Impl;
using ElectionWebServerBot.Models.DomainModel;
using ElectionWebServerBot.Models.Repositories;
using Quartz.Spi;

namespace QuartzNetMvc
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
                 x.For<IUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<ElectionBotEntities1>();
               // x.For<IUnitOfWork>()..Use(() => new ElectionBotEntities1());
                x.For<IRoleRepository>().Use<RoleRepository>();
                x.For<ISatrapInRoleRepository>().Use<SatrapInRoleRepository>();
                x.For<ICandidateRepository>().Use<CandidateRepository>();
                x.For<ISatrapRepository>().Use<SatrapRepository>();
                x.For<IStateRepository>().Use<StateRepository>();
                x.For<IUserRepository>().Use<UserRepository>();
                x.For<IBotStateRepository>().Use<BotStateRepository>();
                x.For<ICityRepository>().Use<CityRepository>();
                x.Scan(s =>
                {
                    s.TheCallingAssembly();
                    s.WithDefaultConventions();
                    s.AddAllTypesOf<IJob>().NameBy(c => c.Name);
                });

                x.SelectConstructor<StdSchedulerFactory>(() => new StdSchedulerFactory());
                x.For<ISchedulerFactory>().Use<StdSchedulerFactory>();
                x.For<IScheduler>().Use(() => ObjectFactory.GetInstance<ISchedulerFactory>().GetScheduler());
            });
            return ObjectFactory.Container;
        }
    }
}

namespace QuartzNetMvc.Jobs
{
    public class StructureMapJobFactory : IJobFactory
    {
        public IJob NewJob(TriggerFiredBundle bundle)
        {
            try
            {
                return (IJob)ObjectFactory.GetInstance(bundle.JobDetail.JobType);
            }
            catch (Exception e)
            {
                var se = new SchedulerException("Problem instantiating class", e);
                throw se;
            }
        }


        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            try
            {
                return (IJob)ObjectFactory.GetInstance(bundle.JobDetail.JobType);
            }
            catch (Exception e)
            {
                var se = new SchedulerException("Problem instantiating class", e);
                throw se;
            }
        }

        public void ReturnJob(IJob job)
        {
            throw new NotImplementedException();
        }
    }
}