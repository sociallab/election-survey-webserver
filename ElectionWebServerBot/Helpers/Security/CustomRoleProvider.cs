﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Web;
using System.Web.Security;
using ElectionWebServerBot.Models.DomainModel;
using ElectionWebServerBot.Models.Repositories;
using System.Web.Mvc;
//TeleTablighApi.Models.DomainModel
namespace Helpers.Security
{
    public class CustomRoleProvider : RoleProvider
    {
        public CustomRoleProvider()
            : this(DependencyResolver.Current.GetService<IUnitOfWork>(), DependencyResolver.Current.GetService<IRoleRepository>(), DependencyResolver.Current.GetService<ISatrapRepository>(), DependencyResolver.Current.GetService<ISatrapInRoleRepository>())
    { }
        readonly IUnitOfWork _uow;
        IRoleRepository repRoles;
        ISatrapRepository repUser;
        ISatrapInRoleRepository repUR;
        public CustomRoleProvider(IUnitOfWork uow, IRoleRepository _repRoles, ISatrapRepository _repUser, ISatrapInRoleRepository _repUR)
        {
            _uow = uow;
            repRoles = _repRoles;
            repUser = _repUser;
            repUR = _repUR;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            if (roleName.Contains(","))
            {
                throw new ArgumentException("Role names cannot contain commas.");
            }

            if (RoleExists(roleName))
            {
                throw new ProviderException("Role name already exists.");
            }
            try
            {
                var r = new ElectionWebServerBot.Models.DomainModel.Role() { Name = roleName };
                repRoles.Add(r);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            int rid = -1;
            var r = repRoles.Where(x => x.Name == roleName).FirstOrDefault();
            if (r != null) rid = r.Id;

            if (rid == -1)
            {
                throw new ProviderException("Role does not exist.");
            }

            if (throwOnPopulatedRole && GetUsersInRole(roleName).Length > 0)
            {
                throw new ProviderException("Cannot delete a populated role.");
            }

            try
            {

                if (repRoles.Delete(rid))
                {
                    foreach (var item in repUR.Where(x => x.RoleId == rid))
                    {
                        if (repUR.Delete(item))
                        {
                            throw new ProviderException("Cannot delete Users in roles.");
                        }
                    }
                  
                }
                else
                {
                    throw new ProviderException("Cannot delete role.");
                }
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            var r = repRoles.Where(x => x.Name == roleName).FirstOrDefault();
            int rid = -1;
            if (r != null)
            {
                rid = r.Id;
            }
            else
            {
                throw new ProviderException("Role does not exist.");
            }
            try
            {
                var rur = repUR.Where(x => x.RoleId == rid).Select(x => x.SatrapId).ToArray();
                var satraps = (from x in repUser.Select()
                               join y in rur on x.Id equals y
                               select x.StrapName).ToArray();
                return satraps;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public override string[] GetAllRoles()
        {
            try
            {
                return repRoles.Select(x => x.Name).ToArray();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1 db = new ElectionWebServerBot.Models.DomainModel.ElectionBotEntities1();
            var user = db.Satraps.Where(x => x.StrapName == username).FirstOrDefault();
            int uid = -1;
            if (user != null)
            {
                uid = user.Id;
            }
            else
            {
                throw new ProviderException("Role does not exist.");

            }
            var r = from x in user.SatrapInRoles
                    from y in db.Roles
                    where x.RoleId == y.Id
                    select y.Name;
            if (r != null)
                return r.ToArray();

            else
                return new string[] { };


        }

        public override string[] GetUsersInRole(string roleName)
        {
            try
            {
                var r = repRoles.Select().FirstOrDefault(x => x.Name.Equals(roleName, StringComparison.CurrentCultureIgnoreCase));
                if (r != null)
                {
                    var result = (from x in r.SatrapInRoles
                                  from y in repUser.Select()
                                  where x.SatrapId == y.Id
                                  select y.StrapName).ToArray();
                    return result;
                }
                else
                {
                    throw new ProviderException("Role morede nazar peida nashood");
                }


            }
            catch (Exception)
            {

                throw;
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return this.GetRolesForUser(username).Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            try
            {
                var r = repRoles.Where(x => x.Name == roleName).FirstOrDefault();
                if (r != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}