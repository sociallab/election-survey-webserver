﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

public static class ViewHelper
{
    private static ConcurrentDictionary<string, IEnumerable<string>> _fileListCache = new ConcurrentDictionary<string, IEnumerable<string>>();

    /// <summary>
    /// Returns each file named Template.* in the given directory wrapped in a templated script tag.
    /// This will cache the list of files in the directory so we do not have to read it on every call
    /// </summary>
    /// <param name="helper"></param>
    /// <param name="directory"></param>
    /// <returns></returns>
    public static MvcHtmlString RenderAllTemplates(this HtmlHelper helper, string directory)
    {

        IEnumerable<string> fileList;

        if (!_fileListCache.TryGetValue(directory, out fileList))
        {
            var physicalPath = helper.ViewContext.RequestContext.HttpContext.Server.MapPath(directory);
            fileList = Directory.GetFiles(physicalPath, "*Template.*")
                                .Select(x => Path.GetFileNameWithoutExtension(x));
            _fileListCache.GetOrAdd(directory, fileList);
        }

        var idPrefix = helper.ViewContext.RouteData.Values["controller"] + "-";
        idPrefix = idPrefix.ToLower();

        var result = string.Join("\n", fileList.Select(x => RenderTemplateToString(helper, idPrefix + GetIdFromPartialViewName(x), x)).ToArray());

        return new MvcHtmlString(result);
    }

    /// <summary>
    /// Returns the given partial view, wrapped in a templated script tag
    /// </summary>
    /// <param name="helper"></param>
    /// <param name="templateId"></param>
    /// <param name="partialViewName"></param>
    /// <returns></returns>
    public static MvcHtmlString RenderTemplate(this HtmlHelper helper, string templateId, string partialViewName)
    {
        var template = RenderTemplateToString(helper, templateId, partialViewName);
        return new MvcHtmlString(template);
    }

    /// <summary>
    /// Returns the partial view to a string and wraps it inside of a templated script tag
    /// </summary>
    /// <param name="helper"></param>
    /// <param name="templateId"></param>
    /// <param name="partialViewName"></param>
    /// <returns></returns>
    private static string RenderTemplateToString(HtmlHelper helper, string templateId, string partialViewName)
    {
        var template = string.Format("<script type=\"text/template\" id=\"{0}\">{1}</script>",
                                     templateId,
                                     RenderPartialToString(helper, partialViewName));

        return template;
    }


    /// <summary>
    /// Returns the partial view and returns the resulting string
    /// </summary>
    /// <param name="helper"></param>
    /// <param name="partialViewName"></param>
    /// <returns></returns>
    private static string RenderPartialToString(HtmlHelper helper, string partialViewName)
    {
        var controllerContext = helper.ViewContext.Controller.ControllerContext;
        var viewData = helper.ViewData;
        var tempData = helper.ViewContext.TempData;

        using (var stringWriter = new StringWriter())
        {
            var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, partialViewName);

            var viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, stringWriter);

            viewResult.View.Render(viewContext, stringWriter);

            return stringWriter.GetStringBuilder().ToString();
        }


    }

    /// <summary>
    /// Creates an id for the script tag element from the partial view name.
    /// This method assumes the partial view name is camel case and the 
    /// resulting id will be all lower case with the words separated by a hyphen
    /// i.e. in: MyFirstTemplate out: my-first-template
    /// </summary>
    /// <param name="partialViewName"></param>
    /// <returns></returns>
    private static string GetIdFromPartialViewName(string partialViewName)
    {
        var result = new StringBuilder();

        for (int i = 0; i < partialViewName.Length; i++)
        {
            if (i > 0 && char.IsUpper(partialViewName[i]))
                result.Append("-").Append(char.ToLower(partialViewName[i]));
            else
                result.Append(char.ToLower(partialViewName[i]));
        }

        return result.ToString();
    }

    //////////////////////////////////////////////////
    public static string RenderPartialToString(this ControllerBase controller)
    {
        return RenderViewToString(controller);
    }
    public static string RenderPartialToString(this ControllerBase controller, string partialViewName)
    {
        IView view = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName).View;
        return RenderViewToString(controller, view);
    }
    public static string RenderPartialToString(this ControllerBase controller, string partialViewName, object model)
    {
        IView view = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName).View;
        return RenderViewToString(controller, view, model);
    }
    public static string RenderPartialViewToString2(this Controller controller, string viewName, object model)
    {
        if (string.IsNullOrEmpty(viewName))
        {
            viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
        }

        controller.ViewData.Model = model;

        using (var sw = new StringWriter())
        {
            // Find the partial view by its name and the current controller context.
            ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

            // Create a view context.
            var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

            // Render the view using the StringWriter object.
            viewResult.View.Render(viewContext, sw);

            return sw.GetStringBuilder().ToString();
        }
    }
    public static string RenderViewToString(this ControllerBase controller)
    {
        string viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
        IView view = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, null).View;
        return RenderViewToString(controller, view);
    }
    public static string RenderViewToString(this ControllerBase controller, string viewName)
    {
        IView view = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, null).View;
        return RenderViewToString(controller, view);
    }
    public static string RenderViewToString(this ControllerBase controller, string viewName, object model)
    {
        IView view = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, null).View;
        return RenderViewToString(controller, view, model);
    }

    public static string RenderViewToString(this ControllerBase controller, IView view)
    {
        using (var writer = new StringWriter())
        {
            var viewContext = new ViewContext(controller.ControllerContext, view, controller.ViewData, controller.TempData, writer);
            view.Render(viewContext, writer);
            return writer.GetStringBuilder().ToString();
        }
    }
    public static string RenderViewToString(this ControllerBase controller, IView view, object model)
    {
        using (var writer = new StringWriter())
        {
            controller.ViewData.Model = model;
            var viewContext = new ViewContext(controller.ControllerContext, view, controller.ViewData, controller.TempData, writer);
            view.Render(viewContext, writer);
            return writer.GetStringBuilder().ToString();
        }
    }
}