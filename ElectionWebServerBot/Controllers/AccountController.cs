﻿using ElectionWebServerBot.Models.DomainModel;
using ElectionWebServerBot.Models.Repositories;
using reCAPTCHA.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ViewModels.Account;

namespace ElectionWebServerBot.Controllers
{
    public class AccountController : Controller
    {
        readonly IUnitOfWork _uow;
        ISatrapRepository RepSatrap;
        // GET: Account
        // GET: Account
        public AccountController(ISatrapRepository _RepSatrap, IUnitOfWork uow)
        {
            _uow = uow;
            RepSatrap = _RepSatrap;
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;

            //if (User.Identity.IsAuthenticated)
            //{
            //    if (shouldRedirect(ReturnUrl))
            //    {
            //        return Redirect(ReturnUrl);
            //    }
            //    return Redirect(FormsAuthentication.DefaultUrl);
            //}

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [CaptchaValidator]
        public ActionResult Login(LoginViewModel user, bool captchaValid, string ReturnUrl)
        {
            //int? pid;
            if (ModelState.IsValid && RepSatrap.Exist(user.Email, user.Password))
            {
                //string uname = (pid.HasValue) ? user.Email + "|" + pid.Value.ToString() : user.Email;
                if (!RepSatrap.Where(x => x.StrapName == user.Email).SingleOrDefault().Enable)
                {
                    return View("ErrorsView", new Tuple<string>(" کاربر" + " ' " + user.Email + " ' " + "غیره فعال می باشد"));

                }
                FormsAuthentication.SetAuthCookie(user.Email, user.RememberMe);
                if (shouldRedirect(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                FormsAuthentication.RedirectFromLoginPage(user.Email, user.RememberMe);
                //return RedirectToAction("Index", "Home");
            }
            else
            {
                if (captchaValid)
                    ViewBag.Message = "نام کاربری یا پسورد اشتباه است";
                else
                    ViewBag.Message = "کپچا به درستی وارد نشده است.";

            }
            return View();
        }
        private bool shouldRedirect(string returnUrl)
        {
            // it's a security check
            return !string.IsNullOrWhiteSpace(returnUrl) &&
                                Url.IsLocalUrl(returnUrl) &&
                                returnUrl.Length > 1 &&
                                returnUrl.StartsWith("/") &&
                                !returnUrl.StartsWith("//") &&
                                !returnUrl.StartsWith("/\\");
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}